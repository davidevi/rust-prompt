
# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.1.1 - 2024-03-04

- Print error if OpenAI authentication is not present instead of crashing

## 0.1.0 - 2024-03-03

- Add ability to send prompts and data to OpenAI
- Add ability to use piped input
- Add ability to authenticate via environment variables
