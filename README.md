# Rust Prompt

CLI tool for ChatGPT interaction

## Usage

Export authentication key for OpenAI
```shell
OPENAI_API_KEY=sk-...
```

```shell
# One-liner
rp --prompt "Do the math" --data "2+2"

# Shorter
rp -p "Do the math" -d "2+2"

# Pipe output as additional data
cat numbers.txt | rp -p "What is the lowest number?" 
```

## Installation

### Homebrew
```shell
brew tap davidevi/homebrew-formulas https://gitlab.com/davidevi/homebrew-formulas.git
```
```shell
brew install davidevi/homebrew-formulas/rust-prompt-0.1.1
```
