use clap::Parser;
use atty::Stream;
use openai_api_rs::v1::api::Client;
use openai_api_rs::v1::chat_completion::{ChatCompletionMessage, ChatCompletionRequest, MessageRole, Content};
use openai_api_rs::v1::common::GPT4;

use std::env;
use std::io::{self, Read};

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    // AI prompt 
    #[arg(short, long)]
    prompt: String,

    // Extra data
    #[arg(short, long)]
    data: Option<String>,
}

fn get_args() -> Args {
    let mut args = Args::parse();
    
    // Load piped data
    if args.data.is_none() && !atty::is(Stream::Stdin) {
        let mut buffer = String::new();
        io::stdin().read_to_string(&mut buffer).expect("Failed to read from stdin");
        args.data = Some(buffer);
    }

    args
}

fn create_request(args: Args, key: String) -> Option<String> {
    let client = Client::new(key);

    let mut messages = vec![
        ChatCompletionMessage {
            role: MessageRole::system,
            content: Content::Text(String::from("You are running in a CLI, follow the user's prompt and apply it on the data (if any)")),
            name: None,
        },
        ChatCompletionMessage {
            role: MessageRole::user,
            content: Content::Text(String::from(args.prompt)),
            name: None
        }];

    if !args.data.is_none() {
        messages.append(&mut vec![
        ChatCompletionMessage {
            role: MessageRole::user,
            content: Content::Text(args.data.unwrap()),
            name: None
        }]);
    }
    
    let request = ChatCompletionRequest::new(
        GPT4.to_string(),
        messages
    );

    let response = client.chat_completion(request).unwrap();
    response.choices[0].message.content.clone()
}

fn main() {
    let key = env::var("OPENAI_API_KEY");

    if key.is_err() {
        println!("No API key found, please set the OPENAI_API_KEY environment variable");
        return;
    }

    let response = create_request(get_args(), key.unwrap());
    if response.is_none() {
        println!("No response from AI");
        return;
    }
    println!("{}", response.unwrap());
}
